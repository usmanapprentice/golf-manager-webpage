Golf Manager Webpage.
Project brief
Technical details:
•	This project requires beginner coding skills. The project should follow HTML5 and CSS3 standards.Website width is 900px and must stay centered no matter how large is the screen. The design issues should be fixed by the coder who takes decisions and he/she is responsible for this changes(he/she  must explain why the changes occur).
•	Browser support: latest versions of Firefox, Chrome and Safari; Internet Explorer 8 and 9;
•	Folder structure:
1.	index.html – contains the HTML code from home.psd filemembership.html – contains the HTML code from membership.psd file
2.	membership.html – contains the HTML code from membership.psd file
1.	CSS folder (/css) contains style.css file
2.	images folder (/images) contains all the images used for this project
3.	javascript folder (/js) contains all the javascript(jQuery) files used for this project starting with main.js that contains the functions loaded after the DOM is ready

3.	Other instructions:
1.	The logo should be a png image;
2.	Drop-down navigation must be made with CSS3 and custom jQuery code in order to have support for all the browser with or without JavaScript enabled;
3.	Home page gallery slider should be made with a jQuery library (eg: http://jqueryui.com/);
4.	Make sure you install the missing fonts (Calibri is not a web safe font);
5.	Social icons: twitter, Facebook, LinkedIn, YouTube are icons that stays fixed no matter how much you scroll down your web page;
6.	Follow the roll-over effect indicated by the hand icon;
7.	Responsive design is out of scope but might be a big plus for the coder. DO NOT waste time with it until you finish the project but keep it in mind when you code your website;
8.	It’s a must to validate your code;
9.	The project should follow the folder structure described above and should contain only 2 HTML pages(index.html and membership.html). However, feel free to explore and add at least one other page that follows the design and typography patterns (this is not a must but it’s a plus). 
